package com.payaut.bookkeeping.resources;

import com.payaut.bookkeeping.dtos.*;
import com.payaut.bookkeeping.services.LedgerService;
import com.payaut.bookkeeping.utils.StringsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LedgerResource {
    @Autowired
    private LedgerService ledgerService;

    @PostMapping(value = "/ledger/credit")
    public ResponseEntity<TransactionResponse> creditOnAccount(@RequestBody TransactionRequest transactionRequest) {
        if (transactionRequest == null) {
            throw new IllegalArgumentException("Transaction request data must be provided");
        }
        TransactionResponse transactionResponse = new TransactionResponse(ledgerService.credit(transactionRequest.getAccountName(), transactionRequest.getAmount()));
        return ResponseEntity.ok(transactionResponse);
    }

    @PostMapping(value = "/ledger/debit")
    public ResponseEntity<TransactionResponse> debitOnAccount(@RequestBody TransactionRequest transactionRequest) {
        if (transactionRequest == null) {
            throw new IllegalArgumentException("Transaction request data must be provided");
        }
        TransactionResponse transactionResponse = new TransactionResponse(ledgerService.debit(transactionRequest.getAccountName(), transactionRequest.getAmount()));
        return ResponseEntity.ok(transactionResponse);
    }

    @PostMapping(value = "/ledger/transfer")
    public ResponseEntity<TransferResponse> transferBetweenAccounts(@RequestBody TransferRequest transferRequest) {
        if (transferRequest == null) {
            throw new IllegalArgumentException("Transfer request data must be provided");
        }
        TransferResponse transferResponse = new TransferResponse(ledgerService.transfer(transferRequest));
        return ResponseEntity.ok(transferResponse);
    }

    @GetMapping(value = "/ledger/balance")
    public ResponseEntity<BalanceResponse> getAccountBalance(@RequestParam String accountName) {
        if (StringsUtil.isNullOrEmpty(accountName)) {
            throw new IllegalArgumentException("An account name must be provided");
        }
        return ResponseEntity.ok(new BalanceResponse(ledgerService.getBalance(accountName, null)));
    }
}
