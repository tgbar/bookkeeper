package com.payaut.bookkeeping.resources;

import com.payaut.bookkeeping.dtos.NewAccountRequest;
import com.payaut.bookkeeping.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountResource  {
    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/account/new", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createAccount(@RequestBody NewAccountRequest newAccountRequest) {
        accountService.createAccount(newAccountRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
