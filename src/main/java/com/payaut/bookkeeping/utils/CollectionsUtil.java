package com.payaut.bookkeeping.utils;

import java.util.Collection;

public final class CollectionsUtil {
    private CollectionsUtil(){}

    public static boolean isNullOrEmpty(final Collection<?> c) {
        return c == null || c.isEmpty();
    }

    /**
     * more helper methods...
     */
}
