package com.payaut.bookkeeping.utils;

public final class StringsUtil {
    private StringsUtil(){}

    public static boolean isNullOrEmpty(String target) {
        return target == null || target.isEmpty();
    }
}
