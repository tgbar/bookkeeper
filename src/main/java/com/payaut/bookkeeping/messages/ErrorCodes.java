package com.payaut.bookkeeping.messages;

public interface ErrorCodes {
    String REQUIRED_FIELD_NOT_FOUND = "required.field.not.found";
}
