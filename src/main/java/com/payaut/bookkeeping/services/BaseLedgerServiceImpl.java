package com.payaut.bookkeeping.services;

import com.payaut.bookkeeping.dtos.TransferRequest;
import com.payaut.bookkeeping.entities.Account;
import com.payaut.bookkeeping.entities.Transaction;
import com.payaut.bookkeeping.repositories.AccountRepository;
import com.payaut.bookkeeping.repositories.LedgerRepository;
import com.payaut.bookkeeping.utils.CollectionsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * In a production-grade application, most of the times, a bookkeeping app is implemented
 * as a double entry bookkeeper. But for this exercise, I'm not taking this into consideration.
 * Keeping it simple as possible, meaning that a debit removes a certain amount from an account
 * and a credit adds to the account
 */
@Service
public class BaseLedgerServiceImpl implements LedgerService {
    private static final long BALANCE_MIN_VALUE = 0L;
    @Autowired
    private LedgerRepository ledgerRepository;
    @Autowired
    private AccountRepository accountRepository;

    /**
     *
     *
     * @param accountName
     * @param amount
     * @return the current balance after executing the debit operation
     * @throws IllegalArgumentException if the provided account doesn't exist in the system
     * @throws IllegalStateException    if the account doesn't have enought balance to debit on
     */
    @Override
    public long debit(@NonNull String accountName, long amount) {
        Account account = checkAndGetAccount(accountName);
        long balance = getBalance(accountName, null);
        if (!isValidBalance(balance, amount)) {
            throw new IllegalStateException("Debit operation cannot be performed on an account without sufficient balance");
        }
        //make transactions immutable
        Transaction transaction = new Transaction.Builder()
                .withAccount(account)
                .withTransactionDate(new Date())
                .withAmount(amount * -1)
                .build();
        ledgerRepository.save(transaction);
        return getBalance(accountName, null);
    }

    /**

     * @return the current balance account
     * @throws IllegalArgumentException if the provided account doesn't exist in the system
     */
    @Override
    public long credit(@NonNull String accountName, long amount) {
        Account account = checkAndGetAccount(accountName);
        Transaction transaction = new Transaction.Builder()
                .withAccount(account)
                .withAmount(amount)
                .withTransactionDate(new Date())
                .build();
        ledgerRepository.save(transaction);
        return getBalance(accountName, null);
    }

    @Transactional
    @NonNull
    @Override
    public long transfer(@NonNull TransferRequest transferRequest) {
        Account senderAccount = checkAndGetAccount(transferRequest.getSender());
        Account recipientAccount = checkAndGetAccount(transferRequest.getRecipient());
        long senderBalance = getBalance(senderAccount.getAccountName(), null);
        long amountToBeTransferred = transferRequest.getAmount();
        if (amountToBeTransferred <= 0) {
            throw new IllegalArgumentException("The amount to be transferred must be a positive value");
        }
        if (!isValidBalance(senderBalance, amountToBeTransferred)) {
            throw new IllegalStateException("Transfer operation cannot be performed because sender account doesn't have enough credit");
        }
        debit(senderAccount.getAccountName(), amountToBeTransferred);
        credit(recipientAccount.getAccountName(), amountToBeTransferred);
        return getBalance(senderAccount.getAccountName(), null);
    }

    /**
     * A balance is calculated by summing all the transaction for a given account
     *
     * @param accountName
     * @return
     */
    @Override
    public long getBalance(@NonNull String accountName, @Nullable Date period) {
        Account account = checkAndGetAccount(accountName);
        List<Transaction> transactions = ledgerRepository.findAllByAccountAccountName(account.getAccountName());
        if (!CollectionsUtil.isNullOrEmpty(transactions)) {
            if (period == null) {
                return transactions.stream()
                        .mapToLong(Transaction::getAmount)
                        .sum();
            }
        }
        return BALANCE_MIN_VALUE;
    }

    /**
     * Check if the provided account exists in the system
     *
     * @param accountName account to be checked
     * @return a valid account in case of success
     * @throws IllegalArgumentException if the provided account doesn't exist in the system
     */
    private Account checkAndGetAccount(@NonNull String accountName) {
        Account account = accountRepository.findByAccountName(accountName);
        if (account == null) {
            throw new IllegalArgumentException("An existing account must be provided");
        }
        return account;
    }

    private boolean isValidBalance(long balance, long amount) {
        return !(balance <= 0 || amount > balance);
    }
}
