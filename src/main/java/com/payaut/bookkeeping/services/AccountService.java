package com.payaut.bookkeeping.services;

import com.payaut.bookkeeping.dtos.NewAccountRequest;
import com.payaut.bookkeeping.entities.Account;
import com.payaut.bookkeeping.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    /**
     * In more complex scenarios, we usually need a mapper or a converter
     * to get all data from input and map to a valid entity object
     *
     * @param newAccountRequest
     */
    public void createAccount(@NonNull NewAccountRequest newAccountRequest) {
        var account = new Account(newAccountRequest.getAccountName());
        accountRepository.save(account);
    }
}
