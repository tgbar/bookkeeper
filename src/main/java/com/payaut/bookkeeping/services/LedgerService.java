package com.payaut.bookkeeping.services;

import com.payaut.bookkeeping.dtos.TransferRequest;

import java.util.Date;

public interface LedgerService {
    long debit(String accountName, long amount);

    long credit(String accountName, long amount);

    long transfer(TransferRequest transferRequest);

    long getBalance(String accountName, Date period);
}
