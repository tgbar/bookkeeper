package com.payaut.bookkeeping.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Transaction {
    @Id
    @GeneratedValue
    private long transactionId;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
    @NotNull
    private long amount;
    @NotNull
    private Date transactionDate;

    public Transaction(){}

    public Transaction(Builder builder) {
        this.account = builder.account;
        this.amount = builder.amount;
        this.transactionDate = builder.transactionDate;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public static class Builder {
        private Account account;
        private long amount;
        private Date transactionDate;

        public Builder withAccount(Account account) {
            this.account = account;
            return this;
        }

        public Builder withAmount(long amount) {
            this.amount = amount;
            return this;
        }

        public Builder withTransactionDate(Date transactionDate) {
            this.transactionDate = transactionDate;
            return this;
        }

        public Transaction build() {
            return new Transaction(this);
        }
    }
}