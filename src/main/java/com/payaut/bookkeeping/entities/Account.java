package com.payaut.bookkeeping.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Account {
    @Id
    @GeneratedValue
    private long accountId;
    @Column(unique = true)
    @NotNull
    private String accountName;
    @OneToMany(mappedBy = "account")
    private List<Transaction> transactions;

    public Account(){}

    public Account(String accountName) {
        this.accountName = accountName;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}