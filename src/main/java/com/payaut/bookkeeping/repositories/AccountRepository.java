package com.payaut.bookkeeping.repositories;

import com.payaut.bookkeeping.entities.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findByAccountName(String accountName);
}
