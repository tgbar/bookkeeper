package com.payaut.bookkeeping.repositories;

import com.payaut.bookkeeping.entities.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LedgerRepository extends CrudRepository<Transaction, Long> {
    List<Transaction> findAllByAccountAccountName(String accountName);
}
