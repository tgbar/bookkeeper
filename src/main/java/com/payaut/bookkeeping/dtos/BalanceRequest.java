package com.payaut.bookkeeping.dtos;

import java.util.Date;

public class BalanceRequest {
    private Date period; // e.g. get balance of last 7 days...

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }
}
