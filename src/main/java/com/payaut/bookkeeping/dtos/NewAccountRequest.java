package com.payaut.bookkeeping.dtos;

import javax.validation.constraints.NotNull;

public class NewAccountRequest {
    @NotNull
    private String accountName;

    public NewAccountRequest(){}

    public NewAccountRequest(@NotNull String accountName) {
        this.accountName = accountName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
