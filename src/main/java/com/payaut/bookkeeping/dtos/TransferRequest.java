package com.payaut.bookkeeping.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class TransferRequest {
    @NotEmpty
    private String sender;
    @NotEmpty
    private String recipient;
    @Min(10) // 10 is equal to 0,01 cent.
    private long amount;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
