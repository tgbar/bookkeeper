package com.payaut.bookkeeping.dtos;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class TransactionRequest {
    @NotEmpty
    private String accountName;
    @Min(10)
    private long amount;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;

    }
}
