package com.payaut.bookkeeping.dtos;

public class BalanceResponse {
    private long balance;

    public BalanceResponse(){}

    public BalanceResponse(long balance) {
        this.balance = balance;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
