package com.payaut.bookkeeping.dtos;

public class TransferResponse {
    private long senderNewBalance;

    public TransferResponse(){}

    public TransferResponse(long senderNewBalance) {
        this.senderNewBalance = senderNewBalance;
    }

    public long getSenderNewBalance() {
        return senderNewBalance;
    }

    public void setSenderNewBalance(long senderNewBalance) {
        this.senderNewBalance = senderNewBalance;
    }
}
