package com.payaut.bookkeeping.dtos;

public class TransactionResponse {
    private long currentBalance;

    public TransactionResponse(){}

    public TransactionResponse(long currentBalance) {
        this.currentBalance = currentBalance;
    }

    public long getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(long currentBalance) {
        this.currentBalance = currentBalance;
    }
}
