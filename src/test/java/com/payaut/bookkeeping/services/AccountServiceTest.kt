package com.payaut.bookkeeping.services

import com.payaut.bookkeeping.dtos.NewAccountRequest
import com.payaut.bookkeeping.entities.Account
import com.payaut.bookkeeping.repositories.AccountRepository
import org.hibernate.HibernateException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AccountServiceTest {
    private var accountRepository = Mockito.mock(AccountRepository::class.java)
    @InjectMocks
    private val accountService = AccountService()

    @BeforeAll
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun whenAccountNameIsNotNull_thenNewAccountIsCreated() {
        val accountName = "PayAut.Merchant1"
        val newAccount = Account()
        val accountRepository = mock(AccountRepository::class.java)
        newAccount.accountId = 1
        newAccount.accountName = accountName
        Mockito.`when`(accountRepository.save(newAccount)).thenReturn(newAccount);
        val newAccountRequest = NewAccountRequest(accountName)
        accountService.createAccount(newAccountRequest)
        Assertions.assertTrue(newAccount.accountId > 0 && newAccount.accountName.equals(newAccountRequest.accountName))
    }

/*    @Test
    fun whenAccountNameIsNotProvided_thenAnExceptionIsThrown() {
        val accountName = ""
        val newAccount = Account()
        newAccount.accountId = 2
        newAccount.accountName = accountName
        Mockito.`when`(accountRepository.save(newAccount)).thenThrow(HibernateException("Unique constraint violated"))
        Assertions.assertThrows(HibernateException::class.java) {
            val newAccountRequest = NewAccountRequest(accountName)
            accountService.createAccount(newAccountRequest)
        }
    }*/
}