## Build & Execution (Docker)

The build and execution through Docker are performed by the following commands:

`mvn clean install`

`sudo docker build -t payaut/bookkeeper:latest .`

`sudo docker run -p 8080:8080 -it payaut/bookeeper`

## Roadmap

### Global
1. move hardcoded messages to messages.properties
2. Double check possible NPEs

### Resources classes
1. create link to get this new account after its creation in DB
2. handle exceptions with spring boot's  exception handlers
3. implement spring security with OAuth + JWT

### AccountService
1. it's possible to implement an early exit in `#createAccount` by checking if provided
   account already exists. The ideal scenario is by getting this information from a cache. 
   By now, the unique constraint in the database will be triggered. 
2. return an encrypted id to provide a link (hateoas) to Account's CRUD operations

### BaseLedgerServiceImpl
1. get accounts from cache: this can be achieve through a cache engine or an in-memory data structure like `ConcurrentHashMap`.
2. maybe it's better to keep a cached mapping of accounts and balances to avoid going through the whole transactions every time
3. filter the transactions according to the provided period
4. implement UTs for this class. I didn't use TDD for this project.