FROM openjdk:11
VOLUME /tmp
EXPOSE 8080
ADD target/bookkeeping-0.0.1-SNAPSHOT.jar bookkeeping.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","bookkeeping.jar"]